$("#pick-one").click(function() {
    if ($("button[data-crud=real]").attr('data-crud') == "real") {
        $("button[data-crud=real]").closest('.right-mode').find('.input-radio').css('display', 'block');
        $("button[data-crud=real]").closest('.right-mode').find('.input-check').css('display', 'none');

    } else {
        if ($(this).attr('data-write') == "action") {
            $("#right-mode").find('.input-radio').css('display', 'block');
            $("#right-mode").find('.input-check').css('display', 'none');
            $("#pick-one-action").prop('checked', true);
            $("#pick-multiple-action").prop('checked', false);
            $('#right-mode').attr('check-type', 'radio');

        }
    }
});
$("#pick-pultiple").click(function() {
    if ($("button[data-crud=real]").attr('data-crud') == "real") {
        $("button[data-crud=real]").closest('.right-mode').find('.input-radio').css('display', 'none');
        $("button[data-crud=real]").closest('.right-mode').find('.input-check').css('display', 'block');

    } else {
        if ($(this).attr('data-write') == "action") {
            $("#right-mode").find('.input-radio').css('display', 'none');
            $("#right-mode").find('.input-check').css('display', 'block');
            $("#pick-one-action").prop('checked', false);
            $("#pick-multiple-action").prop('checked', true)
            $('#right-mode').attr('check-type', 'check');
        }
    }
});
$("#newquestion").click(function() {
    $("#right-mode").clone().appendTo("#canvas");
    $("#right-mode").removeAttr('check-type');
    $("#right-mode").find('input[type=text]').attr('disabled', true);
    $("#likert-scale").find('input').attr('disabled', true);
    $("#right-mode").find('.crud-e').css('display', 'block');
    $("#right-mode").find('*').removeAttr('remake-data');
    $("#right-mode").find('*').removeAttr('id');
    $("#right-mode").find('*').removeAttr('rankink-active');
    $("#right-mode").removeAttr('id');
    $("#right-mode").find('.crud-e').css('display', 'none');
    $("#right-mode").find('select').attr('disabled', false);
    $("#likert-scale").find('input').attr('disabled', false);
    $("#likert-scale").find('input').prop('checked', false);
    $("#sort-answer").find('textarea').val("");
    $("#sort-answer").find('textarea').attr('disabled', false);
    $("#esay").find('textarea').val("");
    $("#esay").find('textarea').attr('disabled', false);
    $("#right-mode").find('.content-input').css('display', 'none');
    $("#right-mode").find('#question').css('display', 'block');
    $("#right-data").find('.content-input').removeClass('content-input');
    $("#right-mode").find('.space-input').addClass('content-input');
    $("#ranking-select select").attr('disabled', false);
    $("#dragable ul li .drag-act").attr('disabled', false);
    $("#drager").removeClass('ui-sortable');
    $("#drager li").removeClass('ui-sortable-handle');
    $("#right-mode").find('.intent').remove();
     $("#to-bank-quest").css('display', 'none');
    for (i = 1; i <= 5; i++) {
        $("#drager .list-drop-d:nth-child(" + [i] + ") label").text([i] + ".");
    };
    $(".drag-drager").sortable({
        revert: true,
    });
    $(".drag-drager li , .drag-drager li *").css('cursor', 'all-scroll');
    $("#question-new").prop('checked', false);
    $(".to-bank-f").prop('checked', false);
    $("#q-title").attr('disabled', false);
    $("#q-data").css('display', 'none');
    $("*[data-write=disabled]").attr('data-write', 'action');
    $("#textarea").attr('disabled', false);
    for (var i = 1; i <= 6; i++) {
        $("#text-" + [i]).attr('disabled', false);
        $("#radio-" + [i]).prop('checked', false);
        $("#check-" + [i]).prop('checked', false);
        $("#text-" + [i]).val('');
    };
    $("#delete").prop('checked', false);
    $('#survey-name:selected').remove();
    $("#textarea").val('');
    $("#survey-name").attr('placeholder', 'Dropdown menu editable');
    $("#textarea").attr('placeholder', 'Question goes here');
    $(this).css('display', 'none');
    $("#modif").css('display', 'none');
    $("#save").css('display', 'block');
    $("#new-option-u").css('display', 'block');
    $("#publish").css('display', 'block');
    for (var i = 1; i <= 6; i++) {
        $("#radio-" + [i]).attr('disabled', false);
        $("#check-" + [i]).attr('disabled', false);
    };
    if ($("#pick-one-action").prop('checked')) {
        for (var i = 1; i <= 6; i++) {
            $("#radio-action-" + [i]).css('display', 'block');
            $("#check-action-" + [i]).css('display', 'none');
        }
    } else if ($("#pick-multiple-action").prop('checked')) {
        for (var i = 1; i <= 6; i++) {
            $("#radio-action-" + [i]).css('display', 'none');
            $("#check-action-" + [i]).css('display', 'block');
        }
    };
    $("#publish").css("display", "none");
});
$("#publish-now").click(function() {
    $("#newquestion").css('display', 'block');
});
$(document).ready(function() {
    $(".input").click(function(event) {
        $(this).closest('.right-mode').find('.input').prop('checked', false);
        $(this).prop('checked', true);
    });
    $(".save").click(function(event) {
        $("textarea").attr("disabled", true);
        $("select").attr('disabled', true);
        $(this).css("display", "none");
        $("#modif").css("display", "block");
        $("#delete").attr("checked", true);
        $("#new-option-u").css("display", "none");
        $("#publish").css("display", "block");
        $("#publish-u").css("display", "block");
        $("#newquestion").css('display', 'block');
        $("#dragable ul li .drag-act").attr('disabled', true);
        $("#dragable ul li .drag-act").css('cursor', 'no-drop');
        $("#q-title").attr('disabled', true);
        $("*[data-write=action]").attr('data-write', 'disabled');
        $("#survey-name").attr('disabled', true);
        $("#textarea").attr('disabled', true);
        $("#right-mode").find('input[type=text]').attr('disabled', true);
        for (var i = 1; i <= 6; i++) {
            $("#text-" + [i]).attr('disabled', true);
            $("#radio-" + [i]).attr('disabled', true);
            $("#check-" + [i]).attr('disabled', true);
        };
        if ($("#question-new").prop('checked') == true) {
            $("#q-data li .data-survey-drop").text($("#survey-name").val());
            $("#q-data li .data-question-name").text($("#q-title").val());
            $("#q-data li .data-question").text($("#question textarea").val());
            for (var i = 1; i <= 6; i++) {
                $("#q-data li .data-answer-" + [i]).text($("#answer-1 input[type=text]").val());
            };
            $("#q-data li .data-sort-answer").text($("#sort-answer textarea").val());
            $("#q-data li .data-esay").text($("#esay textarea").val());
            $("#q-data li").clone().appendTo('#list-chose');
        };
       
    });
});
$(document).ready(function() {
    $(".crud-e").css('display', 'none');
    $(".updated").css('display', 'none');
    $(document).on("click", ".delete", function() {
        $(this).closest(".right-mode").remove();
    });
    $(document).on("click", ".edit", function() {
        $(this).closest(".right-mode").find('textarea').attr('disabled', false);
        $(this).closest(".right-mode").find('input').attr('disabled', false);
        $(this).closest(".right-mode").find('select').attr('disabled', false);
        $(this).parent().css('display', 'none');
        $(this).parent().parent().find('.updated').css('display', 'block');
        $(this).closest('.right-mode').find('.drag-act').attr('disabled', false);
        $(this).closest('.right-mode').find('.q-name').attr('disabled', true);
        $(this).attr('data-crud', 'real');
        $("ul li .drag-act").css('cursor', 'all-scroll');
        $("#survey-name").attr('disabled', false);
    });
    $(document).on("click", ".update", function() {
        $(this).closest(".right-mode").find('textarea').attr('disabled', true);
        $(this).closest(".right-mode").find('input').attr('disabled', true);
        $(this).closest(".right-mode").find('select').attr('disabled', true);
        $(this).parent().css('display', 'none');
        $(this).parent().parent().find('.edited').css('display', 'block');
        $(this).closest('.right-mode').find('.drag-act').attr('disabled', true);
        $(this).closest('.right-mode').find('button[data-crud=real]').attr('data-crud', 'fact');
        $("ul li .drag-act").css('cursor', 'no-drop');
        $("#survey-name").attr('disabled', true);
    });
});
$(".liker-chart-rad").click(function(event) {
    $(this).closest('.likert-chart').find('input').prop('checked', false);
    $(this).prop('checked', true);
});
$("#chose-from-bank").click(function(event) {
    if ($(this).attr('show') == "false") {
        $(this).closest('li').find('#list-chose').css('height', 'auto');
        $(this).attr('show', 'true');
    } else {
        $(this).closest('li').find('#list-chose').css('height', '0px');
        $(this).attr('show', 'false');
    }
});
$(".paste").click(function(event) {
    if ($(this).attr('data-write') == "action") {
        $('#survey-name').val($(this).closest('li').find('.data-survey-drop').text());
        $("#textarea").val($(this).closest('li').find('.data-question').text());
        for (var i = 1; i <= 6; i++) {
            $("#text-" + [i]).val($(this).closest('li').find('.data-answer-1').text());
        };
        $("#text-6").val($(this).closest('li').find('.data-answer-6').text());
        $("#esay").find('textarea').val($(this).closest('li').find('.data-esay').text());
        $("#sort-answer").find('textarea').val($(this).closest('li').find('.data-sort-answer').text());
    }
});
$(".show-hide").click(function(event) {
    if ($("button[data-crud=real]").attr('data-crud') == "real") {
        var action_data = $(this).attr('action-show');
        $("button[data-crud=real]").closest('.right-mode').find('.content-input').css('display', 'none');
        $("button[data-crud=real]").closest('.right-mode').find(action_data).css('display', 'block');
    } else {
        if ($(this).attr('data-write') == "action") {
            var show = $(this).attr('show');
            var hide = $("#question,#likert-scale,#drag-drop,#esay,#sort-answer");
            $(hide).css('display', 'none');
            $(hide).attr('remake-data', false);
            $(show).css('display', 'block');
            $(show).attr('remake-data', true);
        }
    }
});



$(".to-bank-f").change(function(event) {
    if ($(this).prop('checked') == true) {
       $("#to-bank-quest").css('display', 'block');
        $("#to-bank-quest").children('*').css('display', 'block');
        

    } else { 
        $("#to-bank-quest").css('display', 'none');

    }
});



$(document).ready(function() {
    $(".drag-drager").sortable({
        revert: true,
    });
});
$(".new-option-1").click(function() {
    var remake = $('*[remake-data]');

    if ($("*[remake-data=true]").attr('id') == "question") {
        
        if ($("#right-mode").attr('check-type') == "check") {
            $('#clone-to').children('.answer-from-question').clone().appendTo('#answer-data-list');
            $("#right-mode").find('.input-check').css('display', 'block');
            $("#right-mode").find('.input-radio').css('display', 'none');
        }
        else   if ($("#right-mode").attr('check-type') == "radio") {
            $('#clone-to').children('.answer-from-question').clone().appendTo('#answer-data-list');
             $("#right-mode").find('.input-check').css('display', 'none');
            $("#ight-mode").find('.input-radio').css('display', 'block');
        }
        question();
    } else if ($("*[remake-data=true]").attr('id') == "likert-scale") {
        $("#likert-clone").children('div').clone().appendTo('#liker-list-data');
        likert();
    } else if ($("*[remake-data=true]").attr('id') == "drag-drop") {
        if ($("*[rankink-active=true]").attr('id') == "dragable") {
            $("#drag-clone").children('li').clone().appendTo('#drag-list-data');
            drag();
        } else if ($("*[rankink-active=true]").attr('id') == "ranking-select") {
            $("#dropdown-clone").children('div').clone().appendTo('#ranking-select');
            rankink();
        }
    }
});

question();
likert();
drag();
rankink();

function rankink() {
    for (var i = 1; i <= 100; i++) {
        $("#ranking-select").children('.row:nth-child(' + i + ")").find('span.text-icon-gray').text(i + ".")
    }
}

function drag() {
    for (var i = 1; i <= 100; i++) {
        $("#drag-list-data").children('li:nth-child(' + i + ")").find('span.text-icon-gray').text(i + ".")
        $("#drag-list-data").children('li:nth-child(' + i + ")").find('.drag-act span.text-icon-gray').text("option" + " " + i)
    }
}

function likert() {
    for (var i = 1; i <= 100; i++) {
        $("#liker-list-data ").children('.likert-chart:nth-child(' + i + ")").find('h5 span.text-icon-gray').text("option " + i)
    }
}

function question() {
    for (var i = 1; i <= 100; i++) {
        $("#right-mode .answer-from-question:nth-child(" + i + "").find('input[type=text]').attr('placeholder', 'answer ' + i + ' goes here');
    }
}
$(document).on('click', 'div[rankink-active]', function(event) {
    if ($(this).attr('rankink-active') == "false") {
        $(this).closest('#drag-drop').find('div[rankink-active=true]').attr('rankink-active', false);
        $(this).attr('rankink-active', true);
    }
});
$(".js-error").hide();